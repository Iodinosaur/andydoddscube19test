package scoreboard;

import java.util.ArrayList;

public enum SBCommand {
	Score,
	HighestAlltimeScore,
	Board;
	
	public static ArrayList<String> scoreNames = new ArrayList<String>();
	public static ArrayList<String> highestScoreNames = new ArrayList<String>();
	public static ArrayList<String> boardNames = new ArrayList<String>();
	
	public static void initCommands() {
		scoreNames.add("score");
		highestScoreNames.add("highest-alltime-score");
		highestScoreNames.add("highest-score");
		highestScoreNames.add("highest-all-time-score");
		highestScoreNames.add("highestscore");
		highestScoreNames.add("highestalltimescore");
		boardNames.add("board");
	}
	
	public static SBCommand parseCommand(String str) {
		String[] splitCommand = str.split(" ");
		if (scoreNames.contains(splitCommand[0].toLowerCase())) {
			return Score;
		}
		else if (highestScoreNames.contains(splitCommand[0].toLowerCase())) {
			return HighestAlltimeScore;
		}
		else if (boardNames.contains(splitCommand[0].toLowerCase())) {
			return Board;
		}
		
		return null;
	}
}
