/**
 * 
 */
package scoreboard;

import java.util.ArrayList;

/**
 * @author Andy
 *
 */
public class Scoreboard {
	public ArrayList<ScoreboardEntry> scoreboard = new ArrayList<>();
	public int playNumber = 0;
	
	private static final int defaultPosition = -1;
	
	public void score(String username, int score) {
		playNumber++;
		ScoreboardEntry user = findUsername(username);
		int previousPositionOfUser = defaultPosition;
		if (user == null) {
			user = new ScoreboardEntry(username, score, playNumber);
			scoreboard.add(user);
		}
		else {
			user.setScore(score, playNumber);
			previousPositionOfUser = scoreboard.indexOf(user);
		}
		scoreboard.sort(null);
		int newPositionOfUser = scoreboard.indexOf(user);
		
		System.out.println("Play " + playNumber + " - " + generateScoreComment(username, previousPositionOfUser, newPositionOfUser));	
	}
	
	public void board() {
		int rank = 1;
		for (ScoreboardEntry entry : scoreboard) {
			System.out.println(rank + " - " + entry.getUsername() + " " + entry.getScore());
			rank++;
		}
		System.out.println("Current Play - " + playNumber);
	}
	
	public void findHighestScoreForPlayer(String username) {
		ScoreboardEntry user = findUsername(username);
		if (user == null) {
			System.out.println(username + " not found.");
		}
		else {
			System.out.println(username + " reached their highest overall score of " + user.getHighestScore() + " on Play " 
					+ user.getHighestScorePlayNumber());
		}
	}
	
	public void findHighestScore() {
		ScoreboardEntry highestScorer = scoreboard.get(0);
		for (ScoreboardEntry entry : scoreboard) {
			int highScore = entry.getHighestScore();
			if (highScore > highestScorer.getHighestScore()) {
				highestScorer = entry;
			}
		}
		System.out.println(highestScorer.getUsername() + " reached the highest overall score of " + 
			highestScorer.getHighestScore() + " on Play " + highestScorer.getHighestScorePlayNumber());
	}
	
	public ScoreboardEntry findUsername(String username) {
		for (ScoreboardEntry entry: scoreboard) {
			if (entry.getUsername().equals(username)) {
				return entry;
			}
		}
		return null;
	}
	
	public String generateScoreComment(String username, int prevPos, int newPos) {
		if (prevPos == defaultPosition) {
			return username + " enters the leaderboard at rank " + (newPos+1);
		}
		else if (prevPos < newPos) {
			return username + " drops from rank " + (prevPos+1) + " to " + (newPos+1) + " below " + getListOfRankNames(prevPos, newPos - 1);
		}
		else if (prevPos > newPos) {
			return username + " climbs from rank " + (prevPos+1) + " to " + (newPos+1) + " above " + getListOfRankNames(newPos+1, prevPos);
		}
		else {
			return username + " stays at rank " + (newPos+1);
		}
	}
	
	/**
	 * These names can be confusing
	 * refer to the comments to see which value refers to what
	 * @param upperRank the winning rank (ie a lower number)
	 * @param lowerRank the losing rank (ie a higher number)
	 * @return
	 */
	public String getListOfRankNames(int upperRank, int lowerRank) {
		System.out.println("DEBUG: UpperRank: " + upperRank + " LowerRank: " + lowerRank + " List Size: " + scoreboard.size());
		StringBuilder builder = new StringBuilder();
		builder.append(scoreboard.get(upperRank).getUsername());
		for (int i = upperRank+1; i <= lowerRank; i++) {
			builder.append(" and " + scoreboard.get(i).getUsername());
		}
		return builder.toString();
	}


}
