/**
 * 
 */
package scoreboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Andy
 *
 */
public class ScoreboardRunnable {
	
	static boolean active = true;
	static BufferedReader reader;
	
	private Scoreboard scoreboard;

	public static void main(String[] args) {
		ScoreboardRunnable scoreboardRunnable = new ScoreboardRunnable();
		scoreboardRunnable.initInfo();
		
		//Generate a starting message listing the available options
		System.out.println("Welcome to Andy Dodds' Cube19 Coding Test Entry! \n The commands for the scoreboard are as follows:");
		for (SBCommand command: SBCommand.values()) {
			System.out.println(command);
		}
		while (active) {
			try {
				scoreboardRunnable.processSingularCommand();
			}
			catch (IOException e) {
				System.err.println("IOException when processing next command: " + e.getMessage() + "\n" + e.getStackTrace());
			}
			
		}
	}
	
	private void initInfo() {
		//initialise everything needed
		SBCommand.initCommands();
		reader = new BufferedReader(new InputStreamReader(System.in));
		scoreboard = new Scoreboard();
	}
	
	private void processSingularCommand() throws IOException {
		System.out.println("Awaiting input:");
		String commandStr = reader.readLine();
		SBCommand command = SBCommand.parseCommand(commandStr);
		if (command == null) {
			System.out.println(commandStr.split(" ")[0] + " does not exist as a command.");
			return;
		}
		switch (command) {
		case Board:
			scoreboard.board();
			break;
		case HighestAlltimeScore:
			processHighestScore(commandStr);
			break;
		case Score:
			processScore(commandStr);
			break;
		default:
			System.out.println("Unknown Command.");
			break;
		}
	}
	
	private void processScore(String commandStr) {
		String[] splitCmd = commandStr.split(" ");
		if (splitCmd.length == 3) {
			scoreboard.score(splitCmd[1], Integer.parseInt(splitCmd[2]));
		}
		else {
			System.out.println("Score doesn't have that many args. Try username and score instead.");
		}
	}
	
	private void processHighestScore(String commandStr) {
		String[] commandArgs = commandStr.split(" ");
		if (commandArgs.length == 2) {
			scoreboard.findHighestScoreForPlayer(commandArgs[1]);
		}
		else {
			scoreboard.findHighestScore();
		}
	}

}
