/**
 * 
 */
package scoreboard;

/**
 * @author Andy
 *
 */
public class ScoreboardEntry implements Comparable<ScoreboardEntry>{
	private String username;
	private int score;
	private int highestScore;
	private int highestScorePlayNum;

	public ScoreboardEntry(String username, int score, int playNum) {
		this.username = username;
		this.score = score;
		if (score > highestScore) {
			highestScore = score;
			highestScorePlayNum = playNum;
		}
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score, int playNum) {
		this.score = score;
		if (score > highestScore) {
			highestScore = score;
			highestScorePlayNum = playNum;
		}
	}
	
	public int getHighestScore() {
		return highestScore;
	}
	
	public int getHighestScorePlayNumber() {
		return highestScorePlayNum;
	}

	@Override
	public int compareTo(ScoreboardEntry o) {
		return Math.negateExact(((Integer)score).compareTo(o.getScore()));
	}
}








